# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="A kernel-managed devtmpfs mounted on /dev without userspace help"
SLOT="0"
KEYWORDS="~alpha amd64 arm hppa ~ia64 ~m68k ~mips ppc ppc64 s390 sparc x86"
IUSE=""

RDEPEND="!virtual/udev
	!sys-fs/static-dev"

abort() {
	echo
	eerror "devtmpfs must be mounted on /dev."
	die "No devtmpfs detected."
}

pkg_pretend() {
	if [[ ${MERGE_TYPE} == "buildonly" ]] ; then
		# User is just compiling which is fine -- all our checks are merge-time.
		return
	fi

	if [[ ${ROOT} == "/" ]] && \
	   ! awk '$2 == "/dev" && $3 == "devtmpfs" { ok=1 } END { if( !ok ) exit 1 }' /proc/mounts ; then
		abort
	fi
}
